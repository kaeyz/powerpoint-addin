import React, {useEffect, useState} from "react";
import { Image, IImageProps, ImageFit } from 'office-ui-fabric-react/lib/Image';
import axios from "axios";

import data from "./data";
import crate from "./crate.jpeg";


/* const client = axios.create({
  baseURL: "http://localhost:5000",
}); */

const client = axios.create({
  baseURL:"https://step.foryouandyourteam.com/restapiv2",
  auth: {
    username: process.env.REACT_APP_USERNAME,
    password: process.env.REACT_APP_PASSWORD
    },
});

const HeroList = ({ children, onChange })  => {
  const [product, setProduct] = useState({});
  const [img, setImg] = useState("");
  const [isLoading, setIsLoading] = useState(false);

   useEffect(() => {
     //client.get("/product").then(res => setProduct(res.data)).catch(err => console.error(err));

     client.get("/products/pac_adn_prd_192507?context=Global").then(res => console.log(res)).catch(err => console.error(err));

     /* client.get("/img", {
      responseType: 'arraybuffer'
     }).then(res => {
       const buffer = res.data;
      const blob = new Blob( [ buffer ], {type: 'img/jpeg'} );
       setImg(crate);
       setIsLoading(false)
    }).catch(err => console.error(err)); */
  }, []);

  const handleChange = (data, type) => {
    return onChange(data, type);
  };


  return (
    <main className="ms-welcome__main">
      {isLoading ?
        <div>Loading ...</div> :
        <React.Fragment>
          <div style={{ width: '100px', height: '100px' }} onClick= {() => handleChange(img, "image")}>
            <Image
              src={img}
              imageFit={ImageFit.cover}
              maximizeFrame={true}
              alt="crate" />
          </div>
          <p onClick={() => handleChange(product.name, "text")}>{product.name}</p>
          <p onClick={() => handleChange(product.objectType, "text")}>{product.objectType}</p>
        </React.Fragment>
    }
      {children}
    </main>
  );
};

export default HeroList;

