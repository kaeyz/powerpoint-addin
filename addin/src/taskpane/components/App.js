import * as React from "react";
import { Button, ButtonType } from "office-ui-fabric-react";
import Header from "./Header";
import HeroList from "./HeroList";
import Progress from "./Progress";
/* global Button, console, Header, HeroList, HeroListItem, Office, Progress */

export default function App({title, isOfficeInitialized}) {
  const [selected, setSelected] = React.useState("");
  const [type, setType] = React.useState("");

/**
 * Adds a string to powerpoint
 * @param {string} value
 */
  const clickText = async (value) => {
    /**
     * Insert your PowerPoint code here
     */
    Office.context.document.setSelectedDataAsync(
      value,
      {
        coercionType: Office.CoercionType.Text
      },
      result => {
        if (result.status === Office.AsyncResultStatus.Failed) {
          console.error(result.error.message);
        }
      }
    );
  };

  function toDataUrl(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
        var reader = new FileReader();
      reader.onloadend = function () {
        const b64 = reader.result.replace(/^data:image.+;base64,/, '');
            callback(b64);
        }
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
}

  /**
 * Adds a image to powerpoint
 * @param {URL} value
 */
  const clickImage = async (value) => {

    toDataUrl(value, (myBase64) => {
      Office.context.document.setSelectedDataAsync(
        myBase64,
        {
          coercionType: Office.CoercionType.Image,
          imageLeft: 50,
          imageTop: 50,
          imageWidth: 100,
          imageHeight: 100
        },
        result => {
          if (result.status === Office.AsyncResultStatus.Failed) {
            console.error(result.error.message);
          }
        }
      );
   });
};

    if (!isOfficeInitialized) {
      return (
        <Progress title={title} logo="assets/logo-filled.png" message="Please sideload your addin to see app body." />
      );
    }

  const onSelectChange = (data, type) => {
    setType(type);
    return setSelected(data);
  }

  const setToPowerpoint = () => {
    return type === "text" ? clickText(selected) : clickImage(selected);
  };

    return (
      <div className="ms-welcome">
        <Header logo="assets/logo-filled.png" title={title} message="STEP ADDIN" />
        <HeroList onChange={onSelectChange}>
          <p className="ms-font-l">
            Modify the source files, then click <b>Run</b>.
          </p>
          <Button
            className="ms-welcome__action"
            buttonType={ButtonType.hero}
            iconProps={{ iconName: "ChevronRight" }}
            onClick={setToPowerpoint}
          >
            Run
          </Button>
        </HeroList>
      </div>
    );
};
