const data = {
  id: "pac_adn_prd_192507",
  name: "Product 1",
  objectType: "pac_adn_prd",
  parent: "pac_adn_level1_192505",
  values: {},
  references: {
    PrimaryProductImage: {
      reference: {
      contextLocal: true,
      target: "192510",
      targetType: "asset",
      values: {}
    }
  },
  pac_adn_cls: {
    references: [{
      contextLocal: true,
      target: "pac_adn_yl_level1",
      targetType: "classification",
      values: {}
    }]
  }
},
attributeLinks: [],
dataContainers: {}
}

export default Object.freeze(data);