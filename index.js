require("dotenv").config({ path: ".env" });
const express = require("express");
const axios = require("axios");

const cors = require("cors");
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

const client = axios.create({
  baseURL:"https://step.foryouandyourteam.com/restapiv2",
  auth: {
    username: process.env.USERNAME,
    password: process.env.PASSWORD
    },
});

app.get("/product", (req, res) => {
  client.get("/products/pac_adn_prd_192507?context=Global")
    .then(resp => {
      console.log(resp)
    res.status(200).json(resp.data)
  }).catch(err => res.status(400).json("Bad request"));
});


app.get("/img", (req, res) => {
  client.get("/assets/192510/content?context=GL-gl", {
    responseType: 'arraybuffer'
  })
    .then(resp => {
      res.set("Content-Type", "image/jpeg");
      return res.send(Buffer.from(resp.data));
  }).catch(err => console.log(err));
});


app.listen(5000, () => console.log("server started"));