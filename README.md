This is sample app for microsoft Powerpoint addin.

Getting Started
create .env file in "/addin" and "/" and set env values accoding to .env.copy

to run proxy server alone: from root folder run "npm run proxy"

to run addin on web alone: from root folder run "npm run addin:web"

to run addin on web with proxy server : from root folder run "npm run start:web"

to run addin on Powerpoint app alone: from root folder run "npm run addin"

to run addin on Powerpoint app with proxy server : from root folder run "npm run start"